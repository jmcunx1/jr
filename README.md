## jr -- Rearrange Text File Data Columns

jr(1) will rearrange Columns in either a Delimited Text
File or a Flat non-delimited Text File.

This was created for Systems that does not have awk(1)
or col(1)/paste(1).  If awk(1) or col(1)/paste(1) is
present you may want to use those instead.

It can also:
* Adjust Field Formatting, for example convert Date
  Strings.
* Convert to/from Flat Fixed Length Files from/to
  Delimited Text Files.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jr) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jr.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jr.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
